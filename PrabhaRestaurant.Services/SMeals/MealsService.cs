using PrabhaRestaurant.Repository.Domain;
using PrabhaRestaurant.Repository.RMeals;
namespace PrabhaRestaurant.Services.SMeals
{
public class MealsService : GenericService<Meals>, IMealsService
{
public MealsService(IMealsRepository mealsRepository) :
base(mealsRepository)
{
}
}
}