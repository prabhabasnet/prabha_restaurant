using PrabhaRestaurant.Repository.Domain;
using PrabhaRestaurant.Repository.RMenuItems;
namespace PrabhaRestaurant.Services.SMenuItems
{
public class MenuItemsService : GenericService<MenuItems>, IMenuItemsService
{
public MenuItemsService(IMenuItemsRepository menuItemsRepository) :
base(menuItemsRepository)
{
}
}
}