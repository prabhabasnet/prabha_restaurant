using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Services.SMenuItems
{
public interface IMenuItemsService : IGenericService<MenuItems>
{
}
}