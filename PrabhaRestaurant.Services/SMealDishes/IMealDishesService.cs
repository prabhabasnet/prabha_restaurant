using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Services.SMealDishes
{
public interface IMealDishesService : IGenericService<MealDishes>
{
}
}