using PrabhaRestaurant.Repository.Domain;
using PrabhaRestaurant.Repository.RMealDishes;
namespace PrabhaRestaurant.Services.SMealDishes
{
public class MealDishesService : GenericService<MealDishes>, IMealDishesService
{
public MealDishesService(IMealDishesRepository mealDishesRepository) :
base(mealDishesRepository)
{
}
}
}