using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Services.SStaff
{
public interface IStaffService : IGenericService<Staff>
{
}
}