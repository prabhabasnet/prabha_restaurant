using PrabhaRestaurant.Repository.Domain;
using PrabhaRestaurant.Repository.RStaff;
namespace PrabhaRestaurant.Services.SStaff
{
public class StaffService : GenericService<Staff>, IStaffService
{
public StaffService(IStaffRepository staffRepository) :
base(staffRepository)
{
}
}
}