using PrabhaRestaurant.Repository.Domain;
using PrabhaRestaurant.Repository.RMenus;
namespace PrabhaRestaurant.Services.SMenus
{
public class MenusService : GenericService<Menus>, IMenusService
{
public MenusService(IMenusRepository menusRepository) :
base(menusRepository)
{
}
}
}