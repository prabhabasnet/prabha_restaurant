using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Services.SMenus
{
public interface IMenusService : IGenericService<Menus>
{
}
}