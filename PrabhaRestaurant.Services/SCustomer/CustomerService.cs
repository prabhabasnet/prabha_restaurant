using PrabhaRestaurant.Repository.Domain;
using PrabhaRestaurant.Repository.RCustomer;
namespace PrabhaRestaurant.Services.SCustomer
{
public class CustomerService : GenericService<Customer>, ICustomerService
{
public CustomerService(ICustomerRepository customerRepository) :
base(customerRepository)
{
}
}
}