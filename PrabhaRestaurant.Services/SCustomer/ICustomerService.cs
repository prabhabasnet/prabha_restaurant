using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Services.SCustomer
{
public interface ICustomerService : IGenericService<Customer>
{
}
}