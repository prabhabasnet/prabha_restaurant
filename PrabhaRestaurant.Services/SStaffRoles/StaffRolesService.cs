using PrabhaRestaurant.Repository.Domain;
using PrabhaRestaurant.Repository.RStaffRoles;
namespace PrabhaRestaurant.Services.SStaffRoles
{
public class StaffRolesService : GenericService<StaffRoles>, IStaffRolesService
{
public StaffRolesService(IStaffRolesRepository staffRolesRepository) :
base(staffRolesRepository)
{
}
}
}