using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Services.SStaffRoles
{
public interface IStaffRolesService : IGenericService<StaffRoles>
{
}
}