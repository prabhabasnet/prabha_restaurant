using Microsoft.Extensions.DependencyInjection;
using PrabhaRestaurant.Services.SCustomer;
using PrabhaRestaurant.Services.SMealDishes;
using PrabhaRestaurant.Services.SMeals;
using PrabhaRestaurant.Services.SMenuItems;
using PrabhaRestaurant.Services.SMenus;
using PrabhaRestaurant.Services.SStaff;
using PrabhaRestaurant.Services.SStaffRoles;

namespace PrabhaRestaurant.Services
{
public static class ServicesModule
{
public static void Register(IServiceCollection services)
{
services.AddTransient<ICustomerService, CustomerService>();
services.AddTransient<IMenusService, MenusService>();
services.AddTransient<IMenuItemsService, MenuItemsService>();
services.AddTransient<IMealsService, MealsService>();
services.AddTransient<IStaffService, StaffService>();
services.AddTransient<IStaffRolesService, StaffRolesService>();
services.AddTransient<IMealDishesService, MealDishesService>();
}
}
}