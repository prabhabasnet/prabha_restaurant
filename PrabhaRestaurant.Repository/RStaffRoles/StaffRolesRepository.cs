using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Repository.RStaffRoles
{
public class StaffRolesRepository : GenericRepository<StaffRoles>, IStaffRolesRepository
{
public StaffRolesRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}