using PrabhaRestaurant.Repository.Domain;

namespace PrabhaRestaurant.Repository.RStaffRoles
{
public interface IStaffRolesRepository : IGenericRepository<StaffRoles>
{
}
}