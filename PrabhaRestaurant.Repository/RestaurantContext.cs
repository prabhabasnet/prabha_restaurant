using Microsoft.EntityFrameworkCore;
using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Repository
{
public class RestaurantContext : DbContext
{
public RestaurantContext(DbContextOptions<RestaurantContext> options) :
base(options)
{
}
public DbSet<Customer> Customers { get; set; }
public DbSet<Menus> Menus {get; set;}
public DbSet<MenuItems> MenuItems {get; set;}
public DbSet<Meals> Meals {get; set;}
public DbSet<Staff> Staff {get; set;}
public DbSet<StaffRoles> StaffRoles {get; set;}
public DbSet<MealDishes> MealDishes {get; set;}


protected override void OnModelCreating(ModelBuilder modelBuilder)
{
base.OnModelCreating(modelBuilder);
}
}
}