using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Repository.RCustomer
{
public interface ICustomerRepository : IGenericRepository<Customer>
{
}
}