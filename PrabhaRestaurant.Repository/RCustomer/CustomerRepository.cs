using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Repository.RCustomer
{
public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
{
public CustomerRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}