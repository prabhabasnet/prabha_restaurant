using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Repository.RStaff
{
public class StaffRepository : GenericRepository<Staff>, IStaffRepository
{
public StaffRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}