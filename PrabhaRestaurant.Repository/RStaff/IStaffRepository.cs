using PrabhaRestaurant.Repository.Domain;

namespace PrabhaRestaurant.Repository.RStaff
{
public interface IStaffRepository : IGenericRepository<Staff>
{
}
}