using PrabhaRestaurant.Repository.Domain;

namespace PrabhaRestaurant.Repository.RMeals
{
public interface IMealsRepository : IGenericRepository<Meals>
{
}
}