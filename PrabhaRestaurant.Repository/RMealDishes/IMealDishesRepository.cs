using PrabhaRestaurant.Repository.Domain;

namespace PrabhaRestaurant.Repository.RMealDishes
{
public interface IMealDishesRepository : IGenericRepository<MealDishes>
{
}
}