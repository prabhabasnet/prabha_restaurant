using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Repository.RMealDishes
{
public class MealDishesRepository : GenericRepository<MealDishes>, IMealDishesRepository
{
public MealDishesRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}