using PrabhaRestaurant.Repository.Domain;

namespace PrabhaRestaurant.Repository.RMenuItems
{
public interface IMenuItemsRepository : IGenericRepository<MenuItems>
{
}
}