using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Repository.RMenuItems
{
public class MenuItemsRepository : GenericRepository<MenuItems>, IMenuItemsRepository
{
public MenuItemsRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}