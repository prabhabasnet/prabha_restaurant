using PrabhaRestaurant.Repository.Domain;

namespace PrabhaRestaurant.Repository.RMenus
{
public interface IMenusRepository : IGenericRepository<Menus>
{
}
}