using PrabhaRestaurant.Repository.Domain;
namespace PrabhaRestaurant.Repository.RMenus
{
public class MenusRepository : GenericRepository<Menus>, IMenusRepository
{
public MenusRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}