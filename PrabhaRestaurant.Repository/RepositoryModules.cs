using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PrabhaRestaurant.Repository.RCustomer;
using PrabhaRestaurant.Repository.RMenus;
using PrabhaRestaurant.Repository.RMenuItems;
using PrabhaRestaurant.Repository.RStaffRoles;
using PrabhaRestaurant.Repository.RStaff;
using PrabhaRestaurant.Repository.RMeals;
using PrabhaRestaurant.Repository.RMealDishes;
namespace PrabhaRestaurant.Repository
{
public static class RepositoryModule
{
public static void Register(IServiceCollection services, string connection,
string migrationsAssembly)
{
    services.AddDbContext<RestaurantContext>(options =>
    options.UseSqlServer(connection, builder =>
    builder.MigrationsAssembly(migrationsAssembly)));
    
    services.AddTransient<ICustomerRepository, CustomerRepository>();
    services.AddTransient<IMenusRepository, MenusRepository>();
    services.AddTransient<IMenuItemsRepository, MenuItemsRepository>();
    services.AddTransient<IMealsRepository, MealsRepository>();
    services.AddTransient<IStaffRepository, StaffRepository>();
    services.AddTransient<IStaffRolesRepository, StaffRolesRepository>();
    services.AddTransient<IMealDishesRepository, MealDishesRepository>();
    
}
}
}